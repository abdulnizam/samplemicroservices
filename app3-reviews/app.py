from flask import Flask
import requests
import os

app = Flask(__name__)

@app.route('/reviews')
def home():
    ratings_domain = "ratings:9080" if (os.environ.get("RATINGS_DOMAIN") is None) else os.environ.get("RATINGS_DOMAIN")
    with_microservice = "Connect with Microservice" if ratings_domain == "ratings:9080" else "Connect with Gloo gateway"

    response = requests.get('http://{0}/ratings'.format(ratings_domain))
    response.raise_for_status()
    text = response.text
    return {
        "message": 'Hello from Microservice 3 - reviews! ' + text,
        "status": with_microservice
    }

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=9080)
