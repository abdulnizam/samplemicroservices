

# Fault Injection

    https://docs.solo.io/gloo-gateway/latest/getting_started/policy/

    kubectl apply -f fault_injection_policy.yaml -n freniz
    kubectl delete FaultInjectionPolicy faultinjection-basic-delay -n freniz

# Access Policy

    https://docs.solo.io/gloo-mesh-enterprise/latest/policies/access/

    kubectl apply -f access_policies.yaml -n freniz
    kubectl delete AccessPolicy access-policy-basic -n freniz


    Example:

    http://localhost/details/ratings

    http://localhost/reviews


# FailOver
    https://docs.solo.io/gloo-mesh-enterprise/latest/policies/failover/

# Outlier Detection
    https://docs.solo.io/gloo-mesh-enterprise/latest/policies/outlier-detection/

# RetryTimeOut
    https://docs.solo.io/gloo-mesh-enterprise/latest/policies/retry-timeout/



# Notes

    kubectl --context ${REMOTE_CONTEXT1} apply -f {policies-file.yaml}
    
    After config map update:
        kubectl -n freniz rollout restart deployment details

    # Down
        kubectl scale deployment ratings --replicas=0 -n freniz
    # UP
        kubectl scale deployment ratings --replicas=1 -n freniz
