


kubectl apply -f istio/working-sample/template-kubectl.yaml -n freniz
kubectl apply -f istio/working-sample/istio-gateway.yaml -n freniz




 kubectl port-forward svc/microservice1 9080:3000




export INGRESS_HOST=$(minikube ip)
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')


Then you can access the application with http://$INGRESS_HOST:$INGRESS_PORT.




kubectl -n istio-system get service istio-ingressgateway

The EXTERNAL-IP column will contain the IP address to use in the browser.