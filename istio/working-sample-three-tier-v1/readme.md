create namespace 
kubectl create namespace freniz

Enable istio for the created namespace
kubectl label namespace freniz istio-injection=enabled

Follow these two lines for the app deployment and services

kubectl apply -f istio/working-sample-three-tier/template-kubectl.yaml -n freniz
kubectl apply -f istio/working-sample-three-tier/istio-gateway.yaml -n freniz




kubectl port-forward svc/microservice1 4000:9080 -n freniz




export INGRESS_HOST=$(minikube ip)
export INGRESS_PORT=$(kubectl -n istio-system get service istio-ingressgateway -o jsonpath='{.spec.ports[?(@.name=="http2")].nodePort}')


Then you can access the application with http://$INGRESS_HOST:$INGRESS_PORT.




kubectl -n istio-system get service istio-ingressgateway

The EXTERNAL-IP column will contain the IP address to use in the browser.


---------------------------------------

To simulate the scenario where Microservice2 is down and Microservice1 fails over to Microservice3, you can follow these steps:

Scale down the deployment of Microservice2 to 0 replicas:


kubectl scale deployment microservice2-v1 --replicas=0 -n freniz
This command will set the number of replicas of Microservice2 to 0, effectively shutting it down.

Verify that Microservice2 is not running by checking the status of the corresponding pods:


kubectl get pods -l app=microservice2 -n freniz
The output should show 0 pods running for Microservice2.

Access Microservice1 and observe that it automatically fails over to Microservice3 when Microservice2 is down.

You can use the appropriate URL or endpoint to access Microservice1, such as http://localhost:9080/call-microservice2 or the corresponding IP or hostname.

Since Microservice2 is down, Microservice1 will make a request to Microservice3 instead, and you should see the response from Microservice3.

Note: Make sure you have Microservice3 deployed and running before following these steps.

-----------------------------------

To bring Microservice2 back online and switch from Microservice3 to Microservice2, you can follow these steps:

Scale up the deployment of Microservice2 to 1 replica:


kubectl scale deployment microservice2-v1 --replicas=1 -n freniz
This command will set the number of replicas of Microservice2 to 1, allowing it to start running again.

Verify that Microservice2 is running by checking the status of the corresponding pods:


kubectl get pods -l app=microservice2 -n freniz
The output should show 1 pod running for Microservice2.

Access Microservice1 again, and you should see that it is now making requests to Microservice2 instead of Microservice3.

Use the appropriate URL or endpoint to access Microservice1, such as http://localhost:9080/call-microservice2 or the corresponding IP or hostname.

Microservice1 will now communicate with Microservice2, and you should see the response from Microservice2.

By scaling up the deployment of Microservice2, you bring it back online, and Microservice1 will automatically switch to using Microservice2 instead of Microservice3 for requests.

-------------------------------------